from django.http import HttpResponse
from django.shortcuts import render
from .models import Product

# Create your views here.


# /products -> index
def index(request):

    # get all the products from database
    products = Product.objects.all()

    # creating an instance for response to the browser
    # return HttpResponse('Hello World.')

    # return the products to the template view
    return render(request, 'index.html', {
        "products": products
    })


def new(request):
    return HttpResponse('New Products')



