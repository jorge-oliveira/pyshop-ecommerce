from django.contrib import admin
from .models import Product, Offer


# indicate the name on the Products headers on admin django
class ProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'price', 'stock')


# indicate the name on the Offer headers on admin django
class OfferAdmin(admin.ModelAdmin):
    list_display = ('code', 'discount')


# Register your models here.
admin.site.register(Product, ProductAdmin)
admin.site.register(Offer, OfferAdmin)
