from django.urls import path

# import views from current folder
from . import views


#
# /products/1/detail
# /products/new

urlpatterns = [
    path('', views.index),
    path('new', views.new),

]

